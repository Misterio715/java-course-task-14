package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) {
            return null;
        }
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) {
            return null;
        }
        remove(project);
        return project;
    }

    @Override
    public Integer getSize() {
        return projects.size();
    }

    @Override
    public Boolean existsById(final String id) {
        return findOneById(id) != null;
    }

}
