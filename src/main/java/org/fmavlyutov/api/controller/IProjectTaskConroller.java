package org.fmavlyutov.api.controller;

public interface IProjectTaskConroller {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
