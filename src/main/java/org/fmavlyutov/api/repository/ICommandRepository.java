package org.fmavlyutov.api.repository;

import org.fmavlyutov.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
